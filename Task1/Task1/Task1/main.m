//
//  main.m
//  Task1
//
//  Created by Valiantsin Vasiliavitski on 3/24/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTFamily.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
        
        HTPerson* father1 = [HTPerson personWithName:@"Andrew" lastName:@"Gordon" birthday:@"01.03.1863"];
        HTPerson* mother1 = [HTPerson personWithName:@"Marina" lastName:@"Gordon" birthday:@"05.07.1865"];
        HTPerson* son1 = [HTPerson personWithName:@"Alex" lastName:@"Gordon" birthday:@"23.09.1887"];
        
        HTAddress* address1 = [HTAddress addressWithCountry:@"Belarus" city:@"Gomel" street:@"Sovetskaya" zip:40003];
        
        HTFamily* family1 = [[HTFamily alloc] init];
        family1 = [family1 initWIthAddres: address1];
        
        [family1 addMember:father1];
        [family1 addMember:mother1];
        [family1 addMember:son1];
        
        NSLog(@"First family\n");
        
        for(HTPerson* per in family1.members){
            NSLog(@"%@", per);
        }
        
        HTPerson* father2 = [HTPerson personWithName:@"Garfild" lastName:@"Mathew" birthday:@"13.06.1980"];
        HTPerson* mother2 = [HTPerson personWithName:@"Katrin" lastName:@"Mathew" birthday:@"13.02.1990"];
        HTPerson* daughter2 = [HTPerson personWithName:@"Elisabeth" lastName:@"Mathew" birthday:@"17.02.2015"];
        
        HTAddress* address2 = [HTAddress addressWithCountry:@"Belarus" city:@"Minsk" street:@"Telmana" zip:40000];
        
        HTFamily* family2 = [[HTFamily alloc] init];
        family2 = [family2 initWIthAddres: address2];
        
        [family2 addMember:father2];
        [family2 addMember:mother2];
        [family2 addMember:daughter2];
        
        NSLog(@"Secong Family\n");
        
        for(HTPerson* per in family1.members){
            NSLog(@"%@", per);
        }
        
    }
    return 0;
}


