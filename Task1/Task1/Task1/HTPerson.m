//
//  HTPerson.m
//  Task1
//
//  Created by Valiantsin Vasiliavitski on 3/24/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "HTPerson.h"

@interface HTPerson()

@property(nonatomic, readwrite) NSString* fullName;

@end


@implementation HTPerson

//@synthesize fullName;

-(HTPerson*)initWithName:(NSString *)firstName lastName:(NSString *)lastName birthday:(NSString *)birthday {
    
    if(self = [super init]) {
        self.firstName = firstName;
        self.lastName = lastName;
        self.fullName = [[firstName stringByAppendingString:@" "] stringByAppendingString:lastName];
        self.birthday = birthday;
    }
    
    return self;
}

+(HTPerson*)personWithName:(NSString *)firstName lastName:(NSString *)lastName birthday:(NSString *)birthday {
    
    return [[self alloc] initWithName:firstName lastName:lastName birthday:birthday];
}


- (NSString *)description
{
    return [NSString stringWithFormat:@"First name: %@, last name: %@, Birthday: %@", self.firstName, self.lastName, self.birthday];
}



- (nonnull id)copyWithZone:(nullable NSZone *)zone {
    HTPerson* pers = [[[self class] allocWithZone:zone] initWithName:self.firstName lastName:self.lastName birthday:self.birthday];
    return pers;
}

@end
