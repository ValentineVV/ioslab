//
//  HTFamily.h
//  Task1
//
//  Created by Valiantsin Vasiliavitski on 3/24/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTPerson.h"
#import "HTAddress.h"

@interface HTFamily : NSObject

@property(nonatomic) HTAddress *address;
@property(nonatomic, copy) NSArray<HTPerson*> *members;

-(instancetype) initWIthAddres:(HTAddress*) adress;

-(void) addMember:(HTPerson*) member;

@end
