//
//  HTPerson.h
//  Task1
//
//  Created by Valiantsin Vasiliavitski on 3/24/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTPerson : NSObject <NSCopying>

@property(nonatomic, copy) NSString* firstName;
@property(nonatomic, copy) NSString* lastName;
@property(nonatomic, copy, readonly) NSString* fullName;
@property(nonatomic, copy) NSString* birthday;

-(HTPerson*) initWithName:(NSString*) firstName lastName:(NSString*) lastName birthday:(NSString*) birthday;

+(HTPerson*) personWithName:(NSString*) firstName lastName:(NSString*) lastName birthday:(NSString*) birthday;


@end
