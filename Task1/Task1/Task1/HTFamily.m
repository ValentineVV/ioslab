//
//  HTFamily.m
//  Task1
//
//  Created by Valiantsin Vasiliavitski on 3/24/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "HTFamily.h"

@implementation HTFamily

-(instancetype) initWIthAddres:(HTAddress*) address {
    
    if(self = [super init]) {
        self.address = address;
    }
    return self;
}

-(void) addMember:(HTPerson *)member {
    
    NSMutableArray* mutableArray = [[NSMutableArray alloc] init];
    [mutableArray addObjectsFromArray:_members];
    [mutableArray addObject:member];
    self.members = [mutableArray copy];
}

- (NSString *)description
{
    NSMutableString* member = [[NSMutableString alloc] initWithString:@""];
    for (NSString* str in _members){
        
        [member appendString: [str description]];
        [member appendString:@"\n"];
    }
    return [NSString stringWithFormat:@"Address: %@;\n members: %@", _address, member];
}



@end
