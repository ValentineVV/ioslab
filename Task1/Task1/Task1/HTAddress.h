//
//  HTAddress.h
//  Task1
//
//  Created by Valiantsin Vasiliavitski on 3/24/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTAddress : NSObject

@property(nonatomic, copy) NSString* country;
@property(nonatomic, copy) NSString* city;
@property(nonatomic, copy) NSString* street;
@property(nonatomic, assign) NSInteger zip;

-(instancetype) initWithCountry:(NSString*) country city:(NSString*) city street:(NSString*) street zip:(NSInteger) zip;

+(instancetype) addressWithCountry:(NSString*) country city:(NSString*) city street:(NSString*) street zip:(NSInteger) zip;


@end
