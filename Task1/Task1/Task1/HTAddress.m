//
//  HTAddress.m
//  Task1
//
//  Created by Valiantsin Vasiliavitski on 3/24/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "HTAddress.h"

@implementation HTAddress

-(instancetype) initWithCountry:(NSString*) country city:(NSString*) city street:(NSString*) street zip:(NSInteger) zip {
    
    if(self = [super init]) {
        self.country = country;
        self.city = city;
        self.street = street;
        self.zip = zip;
    }
    
    return self;
}

+(instancetype) addressWithCountry:(NSString*) country city:(NSString*) city street:(NSString*) street zip:(NSInteger) zip {
    
    return [[self alloc] initWithCountry:country city:city street:street zip:zip];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Country: %@, city: %@, street: %@, zip: %ld", self.country, self.city, self.street, self.zip];
}

@end
